/**
 * @file		main.c
 * @brief		Zastosowanie biblioteki WiFi do sterowania Zumo Robotem  
 */
#include "MKL46Z4.h"
#include "wifi.h"
#include "leds.h"
#include "fifo.h"
#include "motorDriver2.h"

int main(){

ledsInitialize();
motorDriverInit();
wifiInitialize();
toggleGreen();

while(1){};
}
