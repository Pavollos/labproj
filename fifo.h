/**
 * @file		FiFo.h
 * @brief		FiFo library for KL46Z and ESP8266 module
 */
 
 /**@defgroup FiFo
*@{
*/

#ifndef fifo_h
#define fifo_h
#include <inttypes.h> //zawiera typy uint8_t itd - bez znaku integer 8 bit

#define FIFOBUFSIZE ((uint16_t)250)

#define ERROR -1
#define SUCCESS 0


//Fifo buffer structure
typedef volatile struct {
    uint16_t in;
    uint16_t out;
    uint16_t count;         //ilosc danych w buforze
    char tablica_buf[FIFOBUFSIZE];
} FIFO_BufferTypeDef;

/**
*@brief Cykliczny bufor FIFO
*@var powoluje do zycia 2 bufory do wysylania i odbirania danych
*/
extern FIFO_BufferTypeDef U0Rx, U0Tx;  //deklaracja bufforow UART0

// Deklaracja funkcji
void UART2_Put(uint8_t);
uint8_t UART2_Get(void);

/**
*@brief wpisuje jeden bajt danych do kolejki FIFO
*@param buffer to zmienna typu wskaznik na typ bufora, wiec bedzie to Tx lub Rx
*@param 8 bitowa dana do wpisania
*@retval czy sie powiodlo; w naglowkowym zdefiniowany SUCCESS jako 0 i ERROR jako -1
*/
int FIFO_Put(FIFO_BufferTypeDef* buffer, uint8_t data);

/**
*@brief odczytuje 1 bajt danych z kolejki FIFO
*@param buffer to zmienna typu wskaznik na typ bufora, wiec bedzie to Tx lub Rx
*@param wskaznik na 8 bitowa dana do odczytania
*@retval czy sie powiodlo; w naglowkowym zdefiniowany SUCCESS jako 0 i ERROR jako -1
*/
int FIFO_Get(FIFO_BufferTypeDef* buffer, uint8_t* data);

/**
*@brief usuwa ostatni element w buforze
*@param buffer to zmienna typu wskaznik na typ bufora, wiec bedzie to Tx lub Rx
*@retval czy sie powiodlo; w naglowkowym zdefiniowany SUCCESS jako 0 i ERROR jako -1
*/
int FIFO_Del(FIFO_BufferTypeDef* buffer);

/**
*@brief resetuje bufor
*@param buffer to zmienna typu wskaznik na typ bufora, wiec bedzie to Tx lub Rx
*@retval brak
*/
void FIFO_Init(FIFO_BufferTypeDef* buffer);

/**
*@brief zwraca czy bufor jest pusty czy nie
*@param buffer to zmienna typu wskaznik na typ bufora, wiec bedzie to Tx lub Rx
*@retval czy sie powiodlo; w naglowkowym zdefiniowany SUCCESS jako 0 i ERROR jako -1
*/
int FIFO_IsEmpty(FIFO_BufferTypeDef* buffer);



#endif
