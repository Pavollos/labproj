/** \file
* Zawiera funkcje inicjalizujaca kolejke FiFo
*/

#include "MKL46Z4.h"                     //Device header
#include "fifo.h"												//Declarations
FIFO_BufferTypeDef U0Rx, U0Tx;
void FIFO_Init(FIFO_BufferTypeDef* buffer){
     buffer->in = 0;
     buffer->out = 0;
     buffer->count = 0;
 }
 int FIFO_IsEmpty(FIFO_BufferTypeDef* buffer){
     UART0->C2 &= ~UART_C2_TIE_MASK;
     UART0->C2 &= ~UART_C2_RIE_MASK;
     if (buffer->count == 0){        //jesli nie ma zadnych danych
         UART0->C2 |= UART_C2_RIE_MASK;
         UART0->C2 |= UART_C2_TIE_MASK;
         return SUCCESS;
     }
     else{
         UART0->C2 |= UART_C2_RIE_MASK;
         UART0->C2 |= UART_C2_TIE_MASK;
         return ERROR;
     }
 }
int FIFO_Put(FIFO_BufferTypeDef* buffer, uint8_t data){
     UART0->C2 &= ~UART_C2_TIE_MASK;
     UART0->C2 &= ~UART_C2_RIE_MASK;
     if (buffer->count == FIFOBUFSIZE){ //jesli w tym rejestrze licznik danych jest rowny pojemnosci bufora, to jest pelny, nie da sie nic wpisac => error!
         UART0->C2 |= UART_C2_RIE_MASK;
         UART0->C2 |= UART_C2_TIE_MASK;
         return ERROR;
     }
     ++(buffer->in); //+1 do liczby znajdujacych sie danych wejsciowych
     
     if (buffer->in == FIFOBUFSIZE) //jesli bufor doszedl do konca to wraca do poczatku, FIFO dziala w kolko
         buffer->in = 0;
     ++(buffer->count); //+1 do liczby zawartych danych, bo juz ja wpiszemy na pewno
     buffer->tablica_buf[buffer->in] = data; //w danym rejestrze do tablicy wpiszemy dane na miejsce wyznaczone wczesniej przez buffer->in
    UART0->C2 |= UART_C2_RIE_MASK;
    //UART0->C2 |= UART_C2_TIE_MASK;
     return SUCCESS;
 }
 
int FIFO_Get(FIFO_BufferTypeDef* buffer, uint8_t* data){
     UART0->C2 &= ~UART_C2_TIE_MASK;
     UART0->C2 &= ~UART_C2_RIE_MASK;
     if (buffer->count == 0){
         UART0->C2 |= UART_C2_RIE_MASK;
         UART0->C2 |= UART_C2_TIE_MASK;
         return ERROR;
     }
     ++(buffer->out); //jesli jest cos do odczytania to to odczytam ;p
     
     if (buffer->out == FIFOBUFSIZE) //jesli wskaznik jest ustawiony na ostatni element kolejki FIFO, to przenies na poczatek, bo tam wciaz moga byc elementy, bo on je zapisuje w kolko
         buffer->out = 0;
     *data = buffer->tablica_buf[buffer->out]; //moja dana wpisuje do tablicy na miejsce okreslone wyzej
     --(buffer->count); //odejmuj, bo przeciez odczytalas juz jedna dana
     UART0->C2 |= UART_C2_RIE_MASK;
     UART0->C2 |= UART_C2_TIE_MASK;
     return SUCCESS;
 }
 
int FIFO_Del(FIFO_BufferTypeDef* buffer){
     UART0->C2 &= ~UART_C2_TIE_MASK;
     UART0->C2 &= ~UART_C2_RIE_MASK;
     if (buffer->count == 0){ //musi cos byc by sie dalo to usunac
         UART0->C2 |= UART_C2_RIE_MASK;
         UART0->C2 |= UART_C2_TIE_MASK;
         return ERROR;
     }
     --(buffer->count);
     --(buffer->in);
     if (buffer->in > FIFOBUFSIZE) 
         buffer->in = FIFOBUFSIZE - 1;
     UART0->C2 |= UART_C2_RIE_MASK;
     UART0->C2 |= UART_C2_TIE_MASK;
     return SUCCESS;
 }
 
 void UART0_Put(uint8_t data){
     FIFO_Put(&U0Tx, data); //dodaje dane do buforu
     UART0->C2 |= UART_C2_TIE_MASK;
}

