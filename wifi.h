/**
 * @file		Wifi.h
 * @brief		Wifi library for KL46Z and ESP8266 module
 */
 
 /**@defgroup WiFi
*@{
*/
 
#ifndef wifi_h
#define wifi_h

#include "fifo.h"
#define IloscAT 4//wysylamy 4 komendy AT

/**
*@brief Inicjalizuje modul wifi, wlacza przerwania dla UART0
*@param void
*@retval void
*/
void wifiInitialize(void);

/**
*@brief 
*@param int
*@retval void
*/
int WIFI_Get(FIFO_BufferTypeDef* buffer, char tab[FIFOBUFSIZE], uint8_t ilosc);

/**
*@brief Funkcja wysylajaca w prawidlowej kolejnosci komendy AT
*@param void
*@retval uint8_t
*/
void WyslijAT(uint8_t i);

/**
*@brief Ustawienie modulu w tryb multiple connections. AP.
*@param void
*@retval void
*/
void WyslijAT0(void);

/**
*@brief Ustawienie modulu w tryb serwera, oraz odbior danych na porcie 333
*@param void
*@retval void
*/
void WyslijAT1(void);

/**
*@brief Ustawienie na module kasowania polaczenia po 1sekundzie od nawiazania
*@param void
*@retval void
*/
void WyslijAT2(void);

/**
*@brief Funkcja zmiany SSID, kanalu, enkrypcji i hasla do sieci
*@param void
*@retval void
*/
void WyslijAT3(void);

/**
*@brief Funkcja zmiany SSID, kanalu, enkrypcji i hasla do sieci
*@param void
*@retval void
*/
void WyslijAT4(void);

/**
*@brief Funkcja opozniajaca
*@param void
*@retval uint32_t
*/
void delay(uint32_t value);

/**
*@brief Funkcja wstrzymujaca kolejne komendy AT, az nadejdzie odpowiedz "OK" od modulu.
*@param int
*@retval void
*/
int WIFI_GetAck(void);

void WyslijDane(char IP[], char port[], char dane[], int ilosc);
#endif
