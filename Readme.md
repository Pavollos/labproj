Biblioteka WiFi

Biblioteka do obs�ugi modu�u ESP8266 za pomoc� p�ytki FREEDOM.



Folder zawiera katalogi:

- html- katalog na potrzeby dokumentacji doxygen.

- latex- katalog na potrzeby dokumentacji doxygen.

- strona- katalog dla pliku html.




Katalog g��wny zawiera tak�e podstawowe pliki projektu, za pomoc� kt�rych ca�y projekt pracuje poprawnie.

- fifo- zar�wno .c jak i .h zawieraj� kod do obs�ugi kolejki FiFo.

- wifi- zar�wno .c jak i .h jest to g��wna biblioteka do obs�ugi naszego modu�u. Zawiera instrukcje inicjalizuj�ce, wysy�aj�ce komendy AT, jak i obs�uguj�ce ca�y modu�.



Katalog strona zawiera:

- index- jest to strona g��wna, za pomoc� kt�rej odbywa si� sterowanie robotem.
- jquery- biblioteka do obs�ugi jquery w naszej stronie.
