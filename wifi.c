/** \file
* Zawiera funkcje inicjalizujaca WiFi,ustawienie modulu za pomoca komend AT i obsluge przerwania UART
*/

 
#include "MKL46Z4.h"                    //Device header
#include "wifi.h"												//Declarations
#include "leds.h"
#include "motorDriver2.h"

void delay(uint32_t value){

	uint32_t delay, x;
	
	for(x=0; x < value; x++){
		for(delay=0; delay < 1000; delay++){};
	}
}

volatile uint8_t inicjalizacja;

//string wifi_PobierzStr(FIFO_BufferTypeDef* buffer);
//int wifi_PobierzInt(FIFO_BufferTypeDef* buffer);

void wifiInitialize(void){

	//Wyzerowanie kolejek
	FIFO_Init(&U0Rx);
	FIFO_Init(&U0Tx);
	//UART2 initialize
			SIM->SCGC4 |= SIM_SCGC4_UART0_MASK;
      SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK; //uart0 rx/tx
      PORTA->PCR[2] = PORT_PCR_MUX(2);//tx
      PORTA->PCR[1] = PORT_PCR_MUX(2);//rx
			SIM->SOPT2 |= SIM_SOPT2_UART0SRC(2);
      UART0->C2 &= ~(UART0_C2_RE_MASK | UART0_C2_TE_MASK);//transmisja wylaczona
       
	//BaudRate=115200
      UART0->BDH = UART0_BDH_SBR(0);
      UART0->BDL = UART0_BDL_SBR(17);
			UART0->C5 |= UART0_C5_BOTHEDGE_MASK;		
			UART0->C4 = UART0_C4_OSR(3);
			
      UART0->BDH &= ~UART_BDH_SBNS_MASK;//1bit stopu  
      UART0->C1 &= ~UART_C1_M_MASK;//8bit
			UART0->C1 &= ~UART_C1_PE_MASK;//brak sprawdzania parzystosci
             
						 
      NVIC_ClearPendingIRQ(UART0_IRQn);
      NVIC_EnableIRQ(UART0_IRQn);
			
			inicjalizacja=0;		
			UART0->C2 |= (UART0_C2_TE_MASK | UART0_C2_RE_MASK);//Wlaczamy transmisje
			UART0->C2 |= (UART0_C2_RIE_MASK);//Wlaczamy przerwanie od odbioru, bo modul ma wyslac zwrotne "OK'
			WyslijAT(inicjalizacja);//Rozpoczynamy wysylke IloscAT komend AT
}	
	
void UART0_IRQHandler(void){

	uint8_t znak;
	uint8_t odebrane;
	char pobrane[FIFOBUFSIZE];
	
	
if(UART0->S1 & UART_S1_TDRE_MASK){  //nadajnik jest pusty, wiec moge wysylac to co jest w FIFO
      if(FIFO_Get(&U0Tx, &znak) == SUCCESS){//jesli pobranie sie powiodlo to wysylamy
          UART0->D = znak;
      }else{
          UART0->C2 &= ~UART_C2_TIE_MASK;//jesli nie, to wylaczamy przerwanie od nadawania bo kolejka pusta
      }
  }
    
if(UART0->S1 & UART_S1_RDRF_MASK){   //odbiornik pelny;
	
	odebrane = UART0->D;  //wpisujemy jego zawartosc do zmiennej odebrane
	
	if ((odebrane >= 32 && odebrane < 127)) // sprawdzamy czy litera lub cyfra, to do FIFO wrzucamy
         FIFO_Put(&U0Rx, odebrane);

	switch(odebrane) {
         case 0x0A://0A = 10, wiec to znak nowej linii
					  if(inicjalizacja<IloscAT){//jesli nie wyslalismy jeszcze 4 komend AT to sprawdzamy, czy to potwierdzenie
							if(WIFI_GetAck() == SUCCESS){//Jesli dostalismy potwierdzenie
								WyslijAT(inicjalizacja);//Wysylamy komende AT o numerze [inicjalizacja], nastepnie inkremetujemy zmienna
							}
					 } else {//jesli wyslalismy juz wszystkie komendy, normalna praca

              if(WIFI_Get(&U0Rx, pobrane, 1) == SUCCESS){//pobieramy 1 znak z kolejki &U0Rx do tablicy pobrane
								ProcessCommand(pobrane[0]);
							}
					 }
             break;
         case 0x7f: 
             FIFO_Del(&U0Rx);//7f = 127, wiec to delete
             break;
         case 0x08: 
             FIFO_Del(&U0Rx);//08 = 8, wiec to backspace
             break;
		     default:
             break;
					}
	}
}	

int WIFI_Get(FIFO_BufferTypeDef* buffer, char tab[FIFOBUFSIZE], uint8_t ilosc){
//funkcja bedzie wywolywana na kazdym znaku nowej lini, chcemy by wykryla czy w kolejce jest na poczatku +IPD,
//jesli tak to odebrala [ilosc] danych, jesli nie, to wyczyscila kolejke
uint16_t i;
	for (i=0;; i++) {//for - pobieranie wszystkich danych
       if (FIFO_IsEmpty(buffer) == SUCCESS){ // w pierwszym wolnym miejscu kolejki wpisuje 0 i wyskakuje z petli
           tab[i] = 0;
				 if (i<=17){
				 return ERROR;//koniec za szybko, nie zmiescil sie pakiet danych
				 }
				 break;
       }
       FIFO_Get(buffer, &tab[i]);//zapisujemy znak na ite miejsce w tabeli
			 if (i==3) {//gdy pobierzemy 4 znaki
			  	if ((tab[0] != '+') && (tab[1] != 'I') && (tab[2] != 'P') && (tab[3] != 'D')){//sprawdzamy czy to +IPD
						FIFO_Init(buffer);//jesli nie, czyscimy kolejke i zwracamy ERROR
						return ERROR;
				  }//jesli tak to kontynuujemy pobieranie lini
			 }
   }//Tu pobralismy juz cala linie i zapisalismy do tablicy
	//teraz trzeba uzyc prawie parsera linie
	 for (i=0;i<ilosc; i++){
			tab[i]=tab[i+19];//przesuwamy [ilosc] znakow na poczatek, o 17 bo "+IPD,0,259:GET /?" ma 17 znakow
	 }
	 return SUCCESS;
}
//Jak wygladaja odebrane dane
//0,CONNECT   //ID polaczenia, nawiazanie

//+IPD,0,259:GET /?pin=33 HTTP/1.1
//Host: 192.168.4.1:333
//User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0
//Accept: */*
//Accept-Language: pl,en-US;q=0.7,en;q=0.3
//Accept-Encoding: gzip, deflate
//Origin: null
//Connection: keep-alive

//1,CONNECT   //ID polaczenia, nawiazanie

//+IPD,1,259:GET /?pin=32 HTTP/1.1
//Host: 192.168.4.1:333
//User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0
//Accept: */*
//Accept-Language: pl,en-US;q=0.7,en;q=0.3
//Accept-Encoding: gzip, deflate
//Origin: null
//Connection: keep-alive

//0,CLOSED   //ID polaczenia, koniec
//1,CLOSED   //ID polaczenia, koniec
int WIFI_GetAck(void){
//funkcja bedzie wywolywana na kazdym znaku nowej lini tylko podczas inicjalizacji, chcemy by wykryla "OK" by mozna wyslac nastepna komende,
uint16_t i;
char tab[FIFOBUFSIZE];
	for (i=0; i<2; i++) {
     FIFO_Get(&U0Rx, &tab[i]);//zapisujemy znak na ite miejsce w tabeli
	}
		if ((tab[0] != 'O') && (tab[1] != 'K')){
				FIFO_Init(&U0Rx);//jesli nie, czyscimy kolejke i zwracamy ERROR
				return ERROR;
			}else{
				FIFO_Init(&U0Rx);
				return SUCCESS;//Dostalismy ok
			}

 
}
void WyslijAT(uint8_t i){
switch(i) {
	case 0:
		WyslijAT3();
		break;
	case 1:
		WyslijAT0();
		break;
	case 2:
		WyslijAT1();
		break;
	case 3:
		WyslijAT2();
		break;
}
inicjalizacja++;
UART0->C2 |= (UART0_C2_TIE_MASK);
}
void WyslijAT0(){
//AT+CIPMUX=1    -MultipleCon
	FIFO_Put(&U0Tx, 'A');
	FIFO_Put(&U0Tx, 'T');
	FIFO_Put(&U0Tx, '+');
	FIFO_Put(&U0Tx, 'C');
	FIFO_Put(&U0Tx, 'I');
	FIFO_Put(&U0Tx, 'P');
	FIFO_Put(&U0Tx, 'M');
	FIFO_Put(&U0Tx, 'U');
	FIFO_Put(&U0Tx, 'X');
	FIFO_Put(&U0Tx, '=');
	FIFO_Put(&U0Tx, '1');
	FIFO_Put(&U0Tx, 13);
	FIFO_Put(&U0Tx, 10);
}
void WyslijAT1(){
//AT+CIPSERVER=1,333    -Server, port333
	FIFO_Put(&U0Tx, 'A');
	FIFO_Put(&U0Tx, 'T');
	FIFO_Put(&U0Tx, '+');
	FIFO_Put(&U0Tx, 'C');
	FIFO_Put(&U0Tx, 'I');
	FIFO_Put(&U0Tx, 'P');
	FIFO_Put(&U0Tx, 'S');
	FIFO_Put(&U0Tx, 'E');
	FIFO_Put(&U0Tx, 'R');
	FIFO_Put(&U0Tx, 'V');
	FIFO_Put(&U0Tx, 'E');
	FIFO_Put(&U0Tx, 'R');
	FIFO_Put(&U0Tx, '=');
	FIFO_Put(&U0Tx, '1');
	FIFO_Put(&U0Tx, ',');
	FIFO_Put(&U0Tx, '3');
	FIFO_Put(&U0Tx, '3');
	FIFO_Put(&U0Tx, '3');
	FIFO_Put(&U0Tx, 13);
	FIFO_Put(&U0Tx, 10);
}
void WyslijAT2(){
//AT+CIPSTO=1    -zerwanie polaczenia po 1s
	FIFO_Put(&U0Tx, 'A');
	FIFO_Put(&U0Tx, 'T');
	FIFO_Put(&U0Tx, '+');
	FIFO_Put(&U0Tx, 'C');
	FIFO_Put(&U0Tx, 'I');
	FIFO_Put(&U0Tx, 'P');
	FIFO_Put(&U0Tx, 'S');
	FIFO_Put(&U0Tx, 'T');
	FIFO_Put(&U0Tx, 'O');
	FIFO_Put(&U0Tx, '=');
	FIFO_Put(&U0Tx, '1');
	FIFO_Put(&U0Tx, 13);
	FIFO_Put(&U0Tx, 10);
}

void WyslijAT3(){
 //	AT+CWSAP=ssid,pwd,ch,ecn
	FIFO_Put(&U0Tx, 'A');
	FIFO_Put(&U0Tx, 'T');
	FIFO_Put(&U0Tx, '+');
	FIFO_Put(&U0Tx, 'C');
	FIFO_Put(&U0Tx, 'W');
	FIFO_Put(&U0Tx, 'S');
	FIFO_Put(&U0Tx, 'A');
	FIFO_Put(&U0Tx, 'P');
	FIFO_Put(&U0Tx, '=');
	FIFO_Put(&U0Tx, '"');
	FIFO_Put(&U0Tx, 'R');
	FIFO_Put(&U0Tx, 'o');
	FIFO_Put(&U0Tx, 'b');
	FIFO_Put(&U0Tx, 'o');
	FIFO_Put(&U0Tx, 'c');
	FIFO_Put(&U0Tx, 'i');
	FIFO_Put(&U0Tx, 'k');
	FIFO_Put(&U0Tx, '"');
	FIFO_Put(&U0Tx, ',');
	FIFO_Put(&U0Tx, '"');
	FIFO_Put(&U0Tx, '0');
	FIFO_Put(&U0Tx, '"');
	FIFO_Put(&U0Tx, ',');
	FIFO_Put(&U0Tx, '2');
	FIFO_Put(&U0Tx, ',');
	FIFO_Put(&U0Tx, '0');
	FIFO_Put(&U0Tx, 13);
	FIFO_Put(&U0Tx, 10);
}
void WyslijAT4(){
 //	AT+CWSAP=ssid,pwd,ch,ecn
	FIFO_Put(&U0Tx, 'A');
	FIFO_Put(&U0Tx, 'T');
	FIFO_Put(&U0Tx, '+');
	FIFO_Put(&U0Tx, 'C');
	FIFO_Put(&U0Tx, 'W');
	FIFO_Put(&U0Tx, 'S');
	FIFO_Put(&U0Tx, 'A');
	FIFO_Put(&U0Tx, 'P');
	FIFO_Put(&U0Tx, '=');
	FIFO_Put(&U0Tx, '"');
	FIFO_Put(&U0Tx, 'E');
	FIFO_Put(&U0Tx, 'l');
	FIFO_Put(&U0Tx, 'a');
	FIFO_Put(&U0Tx, '"');
	FIFO_Put(&U0Tx, ',');
	FIFO_Put(&U0Tx, '"');
	FIFO_Put(&U0Tx, '0');
	FIFO_Put(&U0Tx, '"');
	FIFO_Put(&U0Tx, ',');
	FIFO_Put(&U0Tx, '2');
	FIFO_Put(&U0Tx, ',');
	FIFO_Put(&U0Tx, '0');
	FIFO_Put(&U0Tx, 13);
	FIFO_Put(&U0Tx, 10);
}

void WyslijDane(char IP[], char port[], char dane[], int ilosc){
    //IP - tablica 12 znakow, bez kropek. jesli adres -192.168.1.5 trzeba wpisac 1 9 2 1 6 8 0 0 1 0 0 5
    //port - tablica 4 znaki
    //dane - tablica z danymi, pierwsze 1-3 pozycji = ilosc danych, np jesli wysylamy 20 znakow, to na poczatku wpisujemy tu 2 0
    //ilosc - powtornie ilosc danych do wyslania z tablicy dane
    //AT+CIPSTART=id,type,addr,port - nawiazanie polaczenia
    int i;
    FIFO_Put(&U0Tx, 'A');
    FIFO_Put(&U0Tx, 'T');
    FIFO_Put(&U0Tx, '+');
    FIFO_Put(&U0Tx, 'C');
    FIFO_Put(&U0Tx, 'I');
    FIFO_Put(&U0Tx, 'P');
    FIFO_Put(&U0Tx, 'S');
    FIFO_Put(&U0Tx, 'T');
    FIFO_Put(&U0Tx, 'A');
    FIFO_Put(&U0Tx, 'R');
    FIFO_Put(&U0Tx, 'T');
    FIFO_Put(&U0Tx, '=');
    FIFO_Put(&U0Tx, '0');
    FIFO_Put(&U0Tx, ',');
    FIFO_Put(&U0Tx, 'T');
    FIFO_Put(&U0Tx, 'C');
    FIFO_Put(&U0Tx, 'P');
    FIFO_Put(&U0Tx, ',');
    //tu podajemy IP
    for(i=0; i<12; i++){
        FIFO_Put(&U0Tx, IP[i]);
    }
    FIFO_Put(&U0Tx, ',');
    //tu podajemy port
    for(i=0; i<4; i++){
        FIFO_Put(&U0Tx, port[i]);
    }
    FIFO_Put(&U0Tx, 13);
    FIFO_Put(&U0Tx, 10);
   
    //AT+CIPSEND=id,length wysylanie znakow
    FIFO_Put(&U0Tx, 'A');
    FIFO_Put(&U0Tx, 'T');
    FIFO_Put(&U0Tx, '+');
    FIFO_Put(&U0Tx, 'C');
    FIFO_Put(&U0Tx, 'I');
    FIFO_Put(&U0Tx, 'P');
    FIFO_Put(&U0Tx, 'S');
    FIFO_Put(&U0Tx, 'E');
    FIFO_Put(&U0Tx, 'N');
    FIFO_Put(&U0Tx, 'D');
    FIFO_Put(&U0Tx, '=');
    FIFO_Put(&U0Tx, '0');
    FIFO_Put(&U0Tx, ',');
    //tu podajemy dane do wyslania
    for(i=0; i<ilosc; i++){
        FIFO_Put(&U0Tx, dane[i]);
    }
    FIFO_Put(&U0Tx, '.');
    FIFO_Put(&U0Tx, 13);
    FIFO_Put(&U0Tx, 10);
   
    //AT+CIPCLOSE=id - zamykamy polaczenie
    FIFO_Put(&U0Tx, 'A');
    FIFO_Put(&U0Tx, 'T');
    FIFO_Put(&U0Tx, '+');
    FIFO_Put(&U0Tx, 'C');
    FIFO_Put(&U0Tx, 'I');
    FIFO_Put(&U0Tx, 'P');
    FIFO_Put(&U0Tx, 'C');
    FIFO_Put(&U0Tx, 'L');
    FIFO_Put(&U0Tx, 'O');
    FIFO_Put(&U0Tx, 'S');
    FIFO_Put(&U0Tx, 'E');
    FIFO_Put(&U0Tx, '=');
    FIFO_Put(&U0Tx, '0');
}
