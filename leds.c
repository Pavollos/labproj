#include "MKL46Z4.h"                    //Device header
#include "leds.h"												//Declarations

const uint32_t green_mask= 1UL<<5;				//Red led is Port D bit 5
const uint32_t red_mask= 1UL<<29;			//Green led is Port C bit 5/

void ledsInitialize(void) {
	
//Initialize registers	
  SIM->SCGC5 |=  (SIM_SCGC5_PORTD_MASK | SIM_SCGC5_PORTE_MASK);      /* Enable Clock to Port D & E */ 
  PORTD->PCR[5] = PORT_PCR_MUX(1);                       /* Pin PTD5 is GPIO */
  PORTE->PCR[29] = PORT_PCR_MUX(1);                      /* Pin PTE29 is GPIO */
  
	FPTD->PSOR = green_mask	;          /* switch Red LED off */
	FPTE->PSOR = red_mask	;        /* switch Green LED off */
  FPTD->PDDR = green_mask	;          /* enable PTB18/19 as Output */
	FPTE->PDDR = red_mask	;        /* enable PTB18/19 as Output */

}
void toggleRed(void){
	FPTE->PTOR=red_mask;
	}
void toggleGreen(void){
	FPTD->PTOR=green_mask;
	}
void ledRedOn (void) {
	FPTD->PCOR=green_mask;          	/* switch Red LED on */
	FPTE->PSOR=red_mask;          /* switch Green LED off */
}
void ledGreenOn (void) {
	FPTE->PCOR=red_mask;       		/* switch Green LED on */
	FPTD->PSOR=green_mask;          	/* switch Red LED off  */
}
void ledsOff (void) {
		FPTD->PSOR=green_mask;          /* switch Red LED off  */
	  FPTE->PSOR=red_mask;        /* switch Green LED off  */
}
void ledsOn (void) {
		FPTD->PCOR=green_mask;      	/* switch Red LED on  */
	  FPTE->PCOR=red_mask;     	/* switch Green LED on */
}