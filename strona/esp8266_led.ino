    #include <SoftwareSerial.h>
        
    SoftwareSerial esp01(2,3); // komunikacja Rx i Tx z modułem 
    
    void setup()
    {
      Serial.begin(9600);
      esp01.begin(9600); // wartość zależna od modułu, najczęściej 9600 lub 115200
      
      pinMode(11,OUTPUT);    //ustawienie diód LED w stanie niskim
      digitalWrite(11,LOW);
      
      pinMode(12,OUTPUT);
      digitalWrite(12,LOW);
      
      pinMode(13,OUTPUT);
      digitalWrite(13,LOW);
       
      sendData("AT+RST\r\n",2000); // reset modułu
      sendData("AT+CWMODE=2\r\n",1000); // ustawienie w trybie Access Point
      sendData("AT+CIFSR\r\n",1000); // Uzyskanie adresu IP (domyślnie 192.168.4.1)
      sendData("AT+CIPMUX=1\r\n",1000); // Tryb połączeń wielokrotnych
      sendData("AT+CIPSERVER=1,80\r\n",1000); // Ustawienie serwera na porcie 80
    }
     
    void loop()
    {
      if(esp01.available()) // sprawdzenie czy moduł otzrymał dane 
      {
        if(esp01.find("+IPD,"))
        {
         delay(1000); // czekanie na zapełnienie bufora danymi
         
         // odczytanie ID połączenia
         int connectionId = esp01.read()-48; // odjęcie 48 ponieważ funkcja read() zwraca wartość dziesiętną ASCII na pozycji 48
              
         esp01.find("pin="); // wyszukanie frazy "pin=" oraz ustawienie tam "kursora"
         
         int pinNumber = (esp01.read()-48)*10; // pobranie pierwszej cyfry, np. dla pinu 13, pierwsza cyfra to 1, należy ją pomnożyć przez 10
         pinNumber += (esp01.read()-48); // pobranie drugiej cyfry, np. dla pinu 13, druga cyfra to 3, należy ją dodać do poprzedniego wyniku
         
         digitalWrite(pinNumber, !digitalRead(pinNumber)); // zmiana stanu wybranego pinu    
         
         // zamknięcie połączenia
         String closeCommand = "AT+CIPCLOSE="; 
         closeCommand+=connectionId; // ID połączenia odczytane na początku
         closeCommand+="\r\n";
         
         sendData(closeCommand,1000); 
        }
      }
    }
     
    /*
    * Funkcja wysyłająca komendy do modułu ESP01
    * Parametry:
    * command - komenda do wysłania
    * timeout - czas oczekiwania na odpowiedź
    */
    String sendData(String command, const int timeout)
    {
        String response = "";
        
        esp01.print(command); // wysłanie polecenia do ESP01
        
        long int time = millis();
        
        while( (time+timeout) > millis())
        {
          while(esp01.available()) //jeśli są jakieś dane z modułu, wtedy następuje ich odczyt
          {
            char c = esp01.read(); // odczyt kolejnego znaku
            response += c;
          }  
        } 
        Serial.print(response);
        return response;
    }
