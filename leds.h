/**
 * @file		Leds.h
 * @brief		Leds library for KL46Z and ESP8266 module
 */
 
#ifndef leds_h
#define leds_h

void ledsInitialize(void);


void toggleRed(void);
void toggleGreen(void);
void ledsOff (void);
void ledsOn (void);
void ledGreenOff (void);
void ledGreenOn (void);

#endif
